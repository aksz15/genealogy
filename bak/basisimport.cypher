// Alles löschen
CALL apoc.periodic.iterate('MATCH (n) RETURN n', 'DETACH DELETE n', {batchSize:1000});
CALL apoc.schema.assert({},{},true) YIELD label, key RETURN *;

CALL apoc.util.sleep(1000);

//Indexe erstellen

CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.genID);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.properties);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.age);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.born);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.dateOfBirth);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.dateOfDeath);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.yearOfBirth);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.yearOfDeath);

CALL apoc.util.sleep(1000);

//Personenimport
call apoc.periodic.iterate("
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/genealogy/-/raw/master/csv/persons.csv' AS line  FIELDTERMINATOR '|' RETURN line",
"CREATE (p:Person {genID:line.id, properties:line.properties,
familyname:line.familyname,  file:line.file, mother:line.mutter,
born:line.geb, died:line.gest, level:line.level, anchor:line.anchor})",
{batchSize:1000});


CALL apoc.util.sleep(1000);

//KIND-Beziehungen
call apoc.periodic.iterate("
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/genealogy/-/raw/master/csv/parents.csv' AS line  FIELDTERMINATOR '|' RETURN line",
"MATCH (kind:Person {genID:line.kind}), (elternteil:Person {genID:line.elternteil1})
CREATE  (elternteil)<-[:CHILD_OF]-(kind)",
{batchSize:1000});

CALL apoc.util.sleep(1000);

//VERHEIRATET_MIT
call apoc.periodic.iterate("
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/genealogy/-/raw/master/csv/marriage.csv' AS line  FIELDTERMINATOR '|' RETURN line",
"MATCH (p1:Person {genID:line.partner1}), (p2:Person {genID:line.partner2})
CREATE  (p1)-[:SPOUSE]->(p2)",
{batchSize:1000});

CALL apoc.util.sleep(1000);

//SAME_AS
call apoc.periodic.iterate("
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/genealogy/-/raw/master/csv/sameas.csv' AS line  FIELDTERMINATOR '|' RETURN line",
"MATCH (p1:Person {genID:line.a}), (p2:Person {genID:line.b})
CREATE  (p1)-[:SAME_AS]->(p2)",
{batchSize:1000});

CALL apoc.util.sleep(1000);

//EQUAL_ID
call apoc.periodic.iterate("
LOAD CSV WITH HEADERS FROM 'https://git.thm.de/aksz15/genealogy/-/raw/master/csv/nonuniqueids.csv' AS line  FIELDTERMINATOR '|' RETURN line",
"MATCH (p1:Person {genID:line.equal_a}), (p2:Person {genID:line.equal_b})
CREATE  (p1)-[:EQUAL_ID]->(p2)",
{batchSize:1000});

CALL apoc.util.sleep(1000);

//All kids by
LOAD CSV WITH HEADERS FROM 'https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/Genealogy/-/raw/master/csv/momdad.csv' AS line  FIELDTERMINATOR '|'
MATCH (k:Person)-[:CHILD_OF]->(v:Person {genID:line.Vater})-[:SPOUSE]-(m:Person {genID:line.Mutter})
CREATE (k)-[:CHILD_OF]->(m)
RETURN count(*);

CALL apoc.util.sleep(1000);

// genealogyUrl setzen
MATCH (p:Person)
SET p.genealogyUrl = 'http://genealogy.euweb.cz/' + p.familyname + "/" + p.file
WITH p WHERE p.anchor CONTAINS '<'
UNWIND apoc.text.regexGroups(p.anchor, "<A NAME='(.*?)'") as datepart
SET p.genealogyUrl = 'http://genealogy.euweb.cz/' + p.familyname + "/" + p.file + '#' + datepart[1]
RETURN count(*);


CALL apoc.util.sleep(1000);

//Geburtsjahr römisch setzen
MATCH (p1:Person)
WHERE EXISTS(p1.born)
UNWIND apoc.text.regexGroups(p1.born, "([XVI]?[XVI])\\.(\\d\\d\\d\\d?)") as datepart
SET p1.yearOfBirth = substring('0000', 0, 4 - size(datepart[2])) + datepart[2]
RETURN count(*);

CALL apoc.util.sleep(1000);

//Todessjahr römisch setzen
MATCH (p1:Person)
WHERE EXISTS(p1.died)
UNWIND apoc.text.regexGroups(p1.died, "([XVI]?[XVI])\\.(\\d\\d\\d\\d?)") as datepart
SET p1.yearOfDeath = substring('0000', 0, 4 - size(datepart[2])) + datepart[2]
RETURN count(*);

CALL apoc.util.sleep(1000);

//Geburtsdatum setzen
MATCH (p1:Person)
WHERE EXISTS(p1.born)
UNWIND apoc.text.regexGroups(p1.born, "(\\d\\d?)\\.(\\d\\d?)\\.(\\d\\d\\d\\d?)") as datepart
SET p1.dateOfBirth = substring('0000', 0, 4 - size(datepart[3])) + datepart[3]
    + '-'
    + substring('00', 0, 2 - size(datepart[2])) + datepart[2]
    + '-'
    + substring('00', 0, 2 - size(datepart[1])) + datepart[1]
RETURN count(*);

CALL apoc.util.sleep(1000);

//Todesdatum setzen
MATCH (p1:Person)
WHERE EXISTS(p1.died)
UNWIND apoc.text.regexGroups(p1.died, "(\\d\\d?)\\.(\\d\\d?)\\.(\\d\\d\\d\\d?)") as datepart
SET p1.dateOfDeath = substring('0000', 0, 4 - size(datepart[3])) + datepart[3]
    + '-'
    + substring('00', 0, 2 - size(datepart[2])) + datepart[2]
    + '-'
    + substring('00', 0, 2 - size(datepart[1])) + datepart[1]
RETURN count(*);

CALL apoc.util.sleep(1000);

//Geburtsdatum setzen
MATCH (p1:Person)
WHERE EXISTS(p1.born)
UNWIND apoc.text.regexGroups(p1.born, "\\W?(\\d\\d\\d\\d?)") as datepart
SET p1.yearOfBirth = substring('0000', 0, 4 - size(datepart[1])) + datepart[1]
RETURN count(*);

CALL apoc.util.sleep(1000);

//Todesdatum setzen
MATCH (p1:Person)
WHERE EXISTS(p1.died)
UNWIND apoc.text.regexGroups(p1.died, "\\W?(\\d\\d\\d\\d?)") as datepart
SET p1.yearOfDeath = substring('0000', 0, 4 - size(datepart[1])) + datepart[1]
RETURN count(*);

CALL apoc.util.sleep(1000);

//Alter setzen
MATCH (p1:Person)
WHERE EXISTS(p1.yearOfBirth)
AND  EXISTS(p1.yearOfDeath)
SET p1.age = toInteger(p1.yearOfDeath) - toInteger(p1.yearOfBirth)
RETURN count(*);
