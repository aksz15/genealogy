# Verwandtschaftsbeziehungen des europäischen Adels

Auf der Seite http://genealogy.euweb.cz/ werden Verwandtschaftsbeziehungen des europäischen Adels auf HMTL-Seiten zur Verfügung gestellt. Verantwortlich für die Seite ist Miroslav Marek. Wir danken ihm herzlich für die Genehmigung, die Daten hier in diesem Projekt zu verwenden und bereitzustellen.

## [Relavis](https://adwmainz.pages.gitlab.rlp.net/regesta-imperii/lab/relavis/) als Interface zu den Daten
Das Projekt [Relavis](https://adwmainz.pages.gitlab.rlp.net/regesta-imperii/lab/relavis/) bietet einen visuellen Zugang zum Verwandtschaftsgraphen. Den Code und die Credits finden Sie hier https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/relavis.

## Technische Umsetzung und Modellierung der Daten
Die Verwandtschaftsbeziehungen werden mit Hilfe von HTML-Seiten dargestellt. Auf jeder Seite findet sich eine HTML-Nested-List, die der männlichen Linie folgend die nachfolgende Generation immer um eine Stufe einrückt. Beim Vater werden die ggf. mehreren Ehefrauen genannt und durchnummeriert.

[Beispiel](http://genealogy.euweb.cz/hohst/hohenstauf.html)

![Adéle von Vohbourg](https://git.thm.de/aksz15/datascience/-/raw/master/DGWimages/BarbarossaVerwandtschaft.png)

1m: ca 1147 (div 1153) Adéle von Vohbourg (*1122 +1190), dau.of Diepold III, Mgve of Vohbourg; 2m: Wurzburg 10.6.1156 [Béatrice I de Bourgogne](http://genealogy.euweb.cz/ivrea/ivrea1.html#BR3) (*ca 1145 +1184); all kids by 2m.

Die Ehefrauen sind per Hypherlink mit ihren jeweiligen Einträgen auf der Seite ihrer Eltern verknüpft, wenn es eine gibt. Bei den Kindern wird ggf. in eckigen Klammern die Nummer der Ehefrau angegeben, die die Mutter ist (z.B.: [2m.])

Wird eine Seite zu umfangreich bzw. werden die Einrückungen zu tief, wird auf eine neue Seite verlinkt, die dann einen neuen Familienzweig darstellt.

## CSV-Verzeichnis

Im Jahr 2018 hat [Thomas Kollatz](https://www.adwmainz.de/mitarbeiterinnen/profil/drs-thomas-kollatz.html) die Personendaten und deren Relationen aus den HTML-Seiten extrahiert.

### persons.csv

In der Datei persons.csv sind die in den HTML-Dateien enthaltenen Personendaten enthalten. Dabei muss unterschieden werden in Haupteinträge, wie oben in der Abbildung **D1. [1m.] Friedrich III Barbarossa, Duke of Swabia (1147-52),** und Nebeneinträgen wie **2m: Wurzburg 10.6.1156 Béatrice I de Bourgogne (*ca 1145 +1184);**. Nebeneinträge sind mit Haupteinträgen auf anderen Seiten verknüpft, wo die Person in ihrem eigenen Familienstammbaum eingeordnet (sofern vorhanden). Diese Informationen sind in der Datei **sameas.csv** gespeichert.

| Spalte | Erläuterung |
| ------ | ------ |
|id|ID der Person|
|properties|Name|
|familyname|Familienname|
|file|HTML-Datei, in der die Person enthalten ist|
|mutter|Angabe der Mutter, z.B. 0m, 1m oder 2m|
|geb|Geburtsdatum|
|gest|Todesdatum|
|level|Einrückung in der nested Liste|
|anchor|Verweis auf anderen Personendatensatz|

### marriage.csv

Die Datei verbindet den Haupteintrag einer männlichen Person mit der oder den Einträgen der Ehefrau(en). Bei diesen handelt es sich um **Nebeneinträge**, die (falls vorhanden) über SAME_AS-Kanten mit dem Haupteintrag der Person verbunden ist.

| partner1 | partner2 |
| ------ | ------ |
| Id des einen Ehepartners | Id des zweiten Ehepartners |

### parents.csv

Die Datei parents.csv enthält die Id des Elternteils (elternteil1) und des Kindes (kind). Meist handelt es sich um den Vater, da die Listen der männlichen Linie folgen.

| kind | elternteil1 |
| ------ | ------ |
| Id des Kindes | Id des Elternteils |

### sameas.csv

Die Datei sameas.csv verbindet einen Haupteintrag mit einem oder mehreren Nebeneinträgen.

| a | b |
| ------ | ------ |
| Id des einen Personenknotens | Id des zweiten Personenknotens |

In dieser Tabelle werden Ids von Personenknoten zusammengefasst, bei denen man sich **sicher** ist, dass es sich um dieselbe Person handelt.

### nonuniqueids.csv

| equal_a | equal_b |
| ------ | ------ |
| Id des einen Personenknotens | Id des zweiten Personenknotens |

**EQUAL_ID** verbindet Haupteinträge, die den gleichen html-Anker haben. Es handelt sich also nicht um die gleiche Person. Eine der beiden sollte aber eine **SAME_AS-Kante** zu einem anderen Personenknoten haben. Dieser Personenknoten ist dann identisch mit einem der beiden **EQUAL_ID**-Personen.

Hier nochmal ein Beispiel:

![EQUAL_ID in Zusammenhang](https://git.thm.de/aksz15/datascience/-/raw/master/DGWimages/EqualId1.png)

**2m: 1302** könnte als der gleiche sein wie **B2. Duke** oder **D1. Duke**. Hier nochmal das gleich Bild mit den Todesjahren. Es zeigt sich, dass die bestehende **SAME_AS-Kante** richtig ist, die **EQUAL_ID-Kante** kann also gelöscht werden.

![EQUAL_ID mit Lösung](https://git.thm.de/aksz15/datascience/-/raw/master/DGWimages/EqualId2.png)


### momdad.csv

| Mutter | Vater |
| ------ | ------ |
| Id der Mutter aller Kinder des Vaters | Id des Vaters |

In einzelnen Fällen sind die Beziehungen zwischen Mutter und Kind nicht über die 2m-Angabe feststellbar, da im Haupteintrag alle Kinder pauschal einer Ehefrau zugewiesen werden. Beispiel: **2m: Wurzburg 10.6.1156 Béatrice I de Bourgogne (*ca 1145 +1184); all kids by 2m.**
In der Tabelle ist die Id des Vaters und die Id der Mutter aller seiner Kinder enthalten. Mit diesen Informationen können dann die CHILD_OF-Kanten zwischen Kindern und Mutter explizit erstellt werden.

## HTML-Seiten

In den Verzeichnissen **html** finden sich folgende Unterverzeichnisse:

### original-html und genealogy.euweb.cz

In diesen Verzeichnissen befinden sich die heruntergeladenen HTML-Seiten der Seite http://genealogy.euweb.cz auf dem Stand von 2018. Die Codierungen der Seiten können nicht utf8 sein.

### original-utf8

In diesem Verzeichnis befinden sich die Original-HTML-Seiten im utf8-Format.

### bearbeitet-utf8

In diesem Verzeichnis befinden sich korrigierte Fassungen der Original-HTML-Seiten im utf8-Format. Während der Datenaufbereitung sind offensichtliche Tipp- und HTML-Fehler aufgefallen, die in diesen Dateien korrigiert wurden.

## Beispielqueries

Mit dem folgenen Cypherquery werden Muster abgefragt, bei denen Enkel der gleichen Großeltern heiraten

~~~cypher
// Heirat zwischen Enkelkindern
MATCH (p1:Person)-[r:SPOUSE]->(p2:Person)
MATCH (p1:Person)<-[r:SPOUSE]->(p2:Person)
MATCH p=(p1)-[:CHILD_OF*..2]->(g:Person)<-[:CHILD_OF*..2]-(p2)
WHERE length(p)>3
//RETURN p1.name, p2.name, g.name, length(p) LIMIT 20;
RETURN *  LIMIT 20;
~~~

## Lizenz
Die Daten in diesem Repository werden unter CC BY 4.0 Lizenz bereitgestellt. Bitte nennen Sie sowohl Miroslav Marek als auch die Akademie der Wissenschaften und der Literatur, Mainz.
