MATCH (n1:Person)-[s1:SAME_AS]-(n2:Person)
WHERE n1.properties =~ '[A-Z][1-9].*'
AND n1.genID = 'G173019'
AND substring(n1.properties, 4, 4)+"" = substring(n2.properties, 0, 4)+""
AND n1.age = n2.age
//return n1.genID, n1.properties, n2.properties, count(*) as Anzahl order by Anzahl desc
return *;


MATCH (n1:Person)-[s1:SAME_AS]-(n2:Person)
return count(*) as Anzahl, n1.genID, n1.properties, collect(n2.properties) order by Anzahl desc

MATCH (n1:Person)-[s1:SAME_AS]-(n2:Person)
//return count(*) as Anzahl, n1.genID, n1.properties, collect(n2.properties) order by Anzahl desc
where n1.genID in ['G159142', 'G218939', 'G173146']
return *;

match (n1)-[r1:SAME_AS]->(n2)
//match (n1)<-[:SAME_AS]-(n2)
with count(r1) as c, n1, n2
where c > 1
return n1, n2

MATCH (n1)-[r:SAME_AS]->(n2)
where n1.name = n2.name
and n1.age = n2.age
and n1.dateOfDeath = n2.dateOfDeath
return n1.name, n2.name

MATCH (n1)-[r:SAME_AS]->(n2)
where n1.name = n2.name
//and n1.age = n2.age
//and n1.dateOfDeath = n2.dateOfDeath
return n1.genID, n2.genID, n1.name, n2.name, n1.properties, n2.properties, n1.age, n2.age, n1.dateOfDeath, n2.dateOfDeath, n1.dateOfBirth, n2.dateOfBirth, n1.genealogyUrl, n2.genealogyUrl
